#include "UtilsECEO.au3"
Opt("MustDeclareVars", 1) ;0=no, 1=require pre-declaration

#CS Rob Townley
###   For problems, suggestions, pull requests:
###   https://bitbucket.org/eceo/omateupdater/issues
###   https://bitbucket.org/rjt69
###
#CE

;Set the following to the new version of the OfficeMate installation program.
Global $omNewVersion = "12.0.3_SP7"
Global $omFileName = "12.0.3_SP7_Installer.exe"
Global $omDownloadFQFN = "P:\Downloads\OfficeMate12.0\" & $omFileName
Global $omTempFQPN =  "C:\WINDOWS\TEMP\OMATE\" & $omNewVersion & "\"
Global $omTempFQFN =  $omTempFQPN & $omFileName
;local $omOldVersionFQFN = "C:\OfficeMate\omate.exe"
Global $OMSuite = "OfficeMate Service Pack Installer"

local $dtStart = @YEAR & @MON & @MDAY & "-" & @HOUR & @MIN & @SEC
;$dtStart = "" ;for now, lets just have one logfile per machine.
local $timerStart = TimerInit()
local $LogPath = "P:\LogFiles\" & @ScriptName & "\" & @ComputerName & "\"
local $LogFileVerbose  = $LogPath   & $dtStart & "-" & $omNewVersion & "-verbose.LOG"
local $LogFileLess     = $LogPath   & $dtStart & "-" & $omNewVersion & ".LOG"
;LogFileVerbose has more timing info but timing info
;will make it more difficult to diff Log Files on the
;same machine or different machines.  So log to two files.
Global $hLogFileVerbose = FileOpen($LogFileVerbose, 1)
Global $hLogFileLess = FileOpen($LogFileLess, 1)
Local $hndSetupCompleted = -1 ;Stores the handle to the window found when setup is completed to use later for reboot.

#RequireAdmin

SetupLogFiles()
PrintParameters()
SanityChecks()
AmIElevated()
AllowOnlyOneInstance()
PullDownInstallerToLocalDrive()
UnblockFileDeleteStreams()

If 0 > CheckNetworkDrives() Then

   Local $P = DriveMapGet("P:")
   myLog( "P:\ maps to " & $P & "    ErrorCode=" & @error & @CRLF)

   Local $O = DriveMapGet("O:")
   myLog( "O:\ maps to " & $O & "    ErrorCode=" & @error & @CRLF)

   ;Map drives

EndIf


Func UnblockFileDeleteStreamsDownload()
   Local $iR = 0
   Local $sRun = @ComSpec & " /c " & "P:\APPS\Streams.exe /accepteula -s -d " & $omDownloadFQFN ; ; don't forget " " before "/c"
   myLog("RunWait " & $sRun & @CRLF)
   $iR=RunWait($sRun) ; don't forget " " before "/c"
   myLog("RunWait Streams.exe Return=" & $iR & @CRLF)

EndFunc

SanityInfo()
SanityChecks()





#region --- Au3Recorder generated code Start (v3.3.9.5 KeyboardLayout=00000409)  ---

#region --- Internal functions Au3Recorder Start ---
Func _Au3RecordSetup()
Opt('WinWaitDelay',100)
Opt('WinDetectHiddenText',1)
Opt('MouseCoordMode',0)
Local $aResult = DllCall('User32.dll', 'int', 'GetKeyboardLayoutNameW', 'wstr', '')
If $aResult[1] <> '00000409' Then
  MsgBox(64, 'Warning', 'Recording has been done under a different Keyboard layout' & @CRLF & '(00000409->' & $aResult[1] & ')')
EndIf

EndFunc

Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc

myLog("Code generated from Au3Record.exe  " & @CRLF )
_AU3RecordSetup()
#endregion --- Internal functions Au3Recorder End ---



#CS
	OfficeMate Service Pack Installer
	#32770
	Button1
	&Workstation

	How will you use OfficeMate on this computer?
	&Workstation
	&Server
	[Workstation Only: Select this option if the OfficeMate/ExamWRITER software will be used on this computer and will access the OfficeMate data residing on another computer through the network.]
	[Server/Server and Workstation: Select this option if the other computers will access the OfficeMate data on this computer and the OfficeMate/ExamWRITER software may also be used on this computer.]
	&Next >
	Cancel
	Setup Type

   [CLASS:Button; INSTANCE:1]
	ClassnameNN=Static1
	Attention
	#32770
	Database Updated Successfully!

	OfficeMate Service Pack Installer
	[CLASS:Button; INSTANCE:4]
	Finish
	< &Back
	Finish
	Cancel
	OfficeMate Service Pack Installer has finished updating your computer.

	Please click Finish to complete the service pack update  of OfficeMate on your computer!
	Service Pack Install Completed
 #CE

Func LaunchInstaller()
; Launch the officemate installation program.
  Local $hnd;
   ;Run('p:\12sp.bat') ;12sp.bat has already started to map drives and then it calls an executable of this script.
   ;$hnd = _WinWaitActivate("MapDrives.cmd","")
   #Exit from MapDrives.cmd
   ;Send("exit{ENTER}")
   myLog("Sent Exit to cmd.exe, hopefully went to MapDrives.cmd.")


   Run($omTempFQFN)

   Local $OMSuite = "OfficeMate Service Pack Installer"
   _WinWaitActivate($OMSuite,"")
   $hnd = WinWait("OfficeMate Service Pack Installer", _
					"How will you use OfficeMate on this computer?")
   myLog("Selecting Workstation and Next" & @CRLF)
   ControlClick($hnd, _
	  "", _
	  "[CLASS:Button; TEXT:&Workstation >; INSTANCE:1]")
   ControlClick($hnd, _
	  "", _
	  "[CLASS:Button; TEXT:&Next >; INSTANCE:1]")

   $hnd = _WinWaitActivate("Attention","Database Updated Successfully")
   Send("{ENTER}") ;The enter key worked on the keyboard.
;   ControlClick($hnd, "", "[CLASS:Button; TEXT:&OK ; INSTANCE:1]")

   myLog("Database Updated Successfully" & @CRLF)
   ;_WinWaitActivate("OfficeMate Service Pack Installer","OfficeMate Service P")
   ;_WinWaitActivate("OfficeMate Service Pack Installer","OfficeMate Service P")
   ;_WinWaitActivate("OfficeMate Service Pack Installer","OfficeMate Service P")


  myLog("Alot of DLL registration occurs")
  myLog("Please wait, registering OfficeMate DLL files...")

  myLog('Waiting for "Setup Completed" then will Mirror Program Files and sync and click reboot.')
  $hndSetupCompleted = WinWait($OMSuite, _
	  "OfficeMate Service Pack Installer has finished updating your computer." _
	  )
	  ;"Setup Completed")

  MyLog('Now we are immediately before the reboot click is sent, but after "Setup Completed".  Do not want to start mirroring and then reboot before synced.')
  myLog('Will store the handle to the window in hndSetupCompleted, so we can initiate reboot after doing some other things.')
EndFunc ;LaunchInstaller()

Func MirrorProgramFiles()
;Copy the 3D stuff from x64 to x86 or is it vice-versa:
;If x86_64 system, copy from Program Files (x86) to Program Files.
;"c:\Program Files (x86)\3D-Eye Draw"
;"C:\Program Files (x86)\3D-Eye Draw"
   myLog("@ProgramFilesDir=" & @ProgramFilesDir & @CRLF)
   myLog('DirCopy("C:\Program Files (x86)\3D-Eye Draw", "C:\Program Files\3D-Eye Draw",  $FC_OVERWRITE)' )
   FileFlush($hLogFileVerbose)
   FileFlush($hLogFileLess)
   Local $r = -5;
   $r = DirCopy("C:\Program Files (x86)\3D-Eye Draw", "C:\Program Files\3D-Eye Draw",  $FC_OVERWRITE)
   myLog("3D-Eye Draw Copy Returned " & $r & (1 == $r ) ?  ("SUCCESS") : ("FAILURE") & @CRLF)

  FileFlush($hLogFileVerbose)
  FileFlush($hLogFileLess)
  MyLog("Sleep a little to make sure synced.")
  Sleep(30000)

EndFunc ;Mirrorx86 .. x86_64


;~   myLog("Waiting to click Finish.")
;~   $hnd = WinWaitActive($OMSuite, _
;~     "OfficeMate Service Pack Installer has finished updating your computer.")
;~    ControlClick($hnd, _
;~      "Finish", _
;~ 	 "[CLASS:Button; INSTANCE:4]")
;~    myLog("Finish Clicked.")
Func ClickInstallersRebootButton()

  MyLog("About to press OM ReBoot Button")
  MyLog("Waiting to click Finish.")
  $hndSetupCompleted = WinWaitActive($OMSuite, _
    "OfficeMate Service Pack Installer has finished updating your computer.")
   ControlClick($hndSetupCompleted, _
     "Finish", _
	 "[CLASS:Button; INSTANCE:4]")
   myLog("Finish Clicked.")
   myLog("ClickInstallersRebootButton() exiting")
EndFunc

Func TestCompiledProgramWithMissingParameter()
   mylog("Func TestCompiledProgramWithMissingParameter()")
   myLog("Does this error with a bad parameter to a function?")
  ControlClick($hndSetupCompleted, _
     "", _
	 "[CLASS:Button; INSTANCE:4]")
   myLog("What happened to the Omate installer doing a ReBoot.")
   mylog("Func TestCompiledProgramWithMissingParameter()")
EndFunc ;ClickInstallersRebootButton()






myLog("About to LaunchInstaller()")
LaunchInstaller()
MirrorProgramFiles() ;Now we are immediately before the reboot click is sent, but after "Setup Completed".  Do not want to start mirroring and then reboot before sync.


local $executionTime = TimerDiff($timerStart)
myLog( "Execution Time was " & $executionTime & " milliseconds" & @CRLF)
myLog("Rebooting");
ClickInstallersRebootButton()
;TestCompiledProgramWithMissingParameter()
myLog("Calling the command ReStart() as it does not appear Omate does that with SP4?")
ReStart()

