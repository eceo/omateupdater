#include-once  ;
#include <Constants.au3>
#include <MsgBoxConstants.au3>
#include <FileConstants.au3>
#include <File.au3>
#include <Misc.au3>
Opt("MustDeclareVars", 1) ;0=no, 1=require pre-declaration
;#OnAutoItStartRegister "OnAutoItStart"  ; CANNOT REFERENCE ANY VARIABLES DECLARED IN AN INCLUDE!
;Opt("OnExitFunc", "OnAutoItExit")
OnAutoItExitRegister("OnAutoItExit")

#CS Rob Townley
###   For comments, issues and pull requests, create an issue here
###   https://bitbucket.org/eceo/omateupdater/issues
#CE


;Set the following to the new version of the OfficeMate installation program.
;These variables should be declared in the calling script?
;CANNOT REFERENCE ANY VARIABLES DECLARED IN AN INCLUDE!
;But does AutoIT allow EXTERN and GLOBAL and or STATIC.
;local $omTempFQPN =  "C:\WINDOWS\TEMP\OM\" & $omNewVersion & "\"
local $omOmateVersionFQFN = "C:\OfficeMate\omate.exe"
local $omExamWriterVersionFQFN = "C:\OfficeMate\ExamWriter.exe"


local $dtStart = @YEAR & @MON & @MDAY & "-" & @HOUR & @MIN & @SEC
;timerStart and TimerDiff($timerStart) should be in init() and exit().  Yes, but cannot happen in an include file.


local $timerStart = TimerInit()
local $LogPath = "P:\LogFiles\" & @ScriptName & "\"
Global $LogFileVerbose  = $LogPath   & @ComputerName  & "-" & $dtStart & "-verbose.LOG"
Global $LogFileLess     = $LogPath   & @ComputerName  & "-" & $dtStart & ".LOG"
;LogFileVerbose has more timing info but timing info
;will make it more difficult to diff Log Files on the
;same machine or different machines.  So log to two files.


Func ReStart()
#CS
    The shutdown code is a combination of the following values:
	  0 = Logoff
	  1 = Shutdown
	  2 = Reboot
	  4 = Force
	  8 = Power down

	  Add the required values together. To shutdown and power down, for example,
		 the code would be 9 (shutdown + power down = 1 + 8 = 9).

	  Standby or hibernate can be achieved with third-party software such as
	  http://grc.com/wizmo/wizmo.htm
#CE
   local $rc = Shutdown(2)
   mylog("@error=" & @error)
   if 0 = $rc Then
	  myLog("Shutdown failed, trying again with 2 + 4 or 6 to Shutdown()" & @CRLF)
	  mylog("@error=" & @error)
	  local $rc2 = Shutdown(6)
	  if 0 = $rc2 Then
	     myLog("Shutdown(6) failed as well." & @CRLF)
		 mylog("@error=" & @error)
	  ElseIf 1 = $rc2 Then
		 myLog("Shutdown(6) successfully called, but second attempt." & @CRLF )
	  Else
		 myLog("Shutdown(6) is only supposed to return zero or one, but it returned something different." & @CRLF)
		 mylog("@error=" & @error)
	  EndIf
   ElseIf 1 = $rc Then
	  myLog("Shutdown(2) successfully called." & @CRLF )
	  mylog("@error=" & @error)
   Else
	  myLog("Shutdown() is only supposed to return zero or one, but it returned something different." & @CRLF)
	  mylog("@error=" & @error)
   EndIf
   mylog("@error=" & @error)
EndFunc

Func myLog($s)
   _FileWriteLog($LogFileVerbose, $s)
   FileWriteLine($LogFileLess, $s)
EndFunc


Func SetupLogFiles()
   DirCreate($LogPath)
   myLog( "DirCreate(" & $LogPath & ")  Error=" & @error & @CRLF)
   myLog( "LogFileVerbose=" & $LogFileVerbose & @CRLF)
   myLog( "LogFileLess=" & $LogFileLess & @CRLF)
   myLog( "Start Time=" & $dtStart & @CRLF)
   myLog( "ComputerName=" & @ComputerName & @CRLF)
   myLog( "UserName=" & @UserName & @CRLF)
EndFunc

Func AmIElevated()
   ;Not sure this works when inside a function .... good chance it will not!
   #RequireAdmin
   If IsAdmin() Then
     myLog("Admin rights are detected." & @CRLF)
   Else
     myLog("COULD NOT ELEVATE " & @UserName & "! ADMIN RIGHTS NOT DETECTED!" & @CRLF)
	 Exit
  EndIf
EndFunc

Func OnAutoItExit()

   local $executionTime = TimerDiff($timerStart)
   myLog( "Execution Time was " & $executionTime & " milliseconds" & @CRLF)


	;Inside the function, @ExitCode can be used to retrieve the code set by the exit statement.
	;The mode of exit can be retrieved with @ExitMethod.
	Global Const $arrExits[10][80] = [[0, 1, 2, 3, 4, 1025],["0 Natural closing","1 close by Exit function","2 close by clicking on exit of the systray","3 close by user logoff","4 close by Windows shutdown","1025 Close by HotKeyExit()",""]]
	myLog(@ScriptFullPath & " ExitScript() called with @EXITMETHOD=" & @EXITMETHOD)
	Exit(0)
EndFunc

Func AllowOnlyOneInstance()
   If _Singleton(@ComputerName, 2) = 0 Then
	  myLog( "Already running on this machine, aborting." & @CRLF)
	  Exit
   Else
	  myLog( "Good. No other instance of this script found. Continuing" & @CRLF)
   EndIf
EndFunc

Func PrintParameters()
   local $i
   local $sParams = ""
   for  $i = 0 to $CmdLine[0] step 1
	  $sParams &= $i & "=" & $CmdLine[$i] & "   "

   Next
   myLog($sParams)
EndFunc

Func CheckNetworkDrives()
   local $r
  Local $sOstatus = DriveStatus( "O:" & "\") ; Find the status of the home drive, generally this is the O:\ drive.
  Local $sPstatus = DriveStatus( "P:" & "\")
  myLog("O:\ = " & $sOstatus & @CRLF & "P:\ = " & $sPstatus & @CRLF)
  if "READY" == $sOstatus And "READY" == $sPstatus then
	 myLog( "Network drives check out OK." & @CRLF )
	 $r = 1
  Else
	 myLog( "Problem with O:\ or P:\ drives!" & @CRLF)
     $r = -1
  EndIf
  Return($r)
EndFunc

Func SanityInfo()
  myLog("@ComputerName=" & @ComputerName & @CRLF)
  myLog("@CPUArch=" & @CPUArch & " " & "@OSArch=" & @OSArch & " " & "@OSBuild=" & @OSBuild  & @CRLF)
  myLog("OSVersion=" & @OSVersion & " OSServicePack=" & @OSServicePack & @CRLF)
EndFunc

Func SanityChecks()
   CheckNetworkDrives()
   myLog("OMATE.exe version is: " & FileGetVersion($omOmateVersionFQFN) & @CRLF);
   myLog("ExamWriter.exe is version: "  & FileGetVersion($omExamWriterVersionFQFN) & @CRLF)
EndFunc


Func MakeAlwaysOnTop()
    ; Retrieve the handle of the active window.
    Local $hWnd = WinGetHandle("[ACTIVE]")

    ; Set the active window as being ontop using the handle returned by WinGetHandle.
    WinSetOnTop($hWnd, "", 1)

    ; Wait for 2 seconds to display the change.
    Sleep(2000)

    ; Remove the "topmost" state from the active window.
    WinSetOnTop($hWnd, "", 0)
 EndFunc   ;==>MakeAlwaysOnTop

 Func UnblockFileDeleteStreams()
	;This is not working any longer, there must be another stream to edit.
	;PowerShell.exe -v4 has the unblock-file cmdlet.
	myLog("omTempFQPN=" & $omTempFQPN)
   Local $iR = 0
   Local $sRun = @ComSpec & " /c " & "P:\APPS\Streams.exe /accepteula -s -d " & $omTempFQPN ; ; don't forget " " before "/c"
   myLog("RunWait " & $sRun & @CRLF)
   $iR=RunWait($sRun) ; don't forget " " before "/c"
   myLog("RunWait Streams.exe Return=" & $iR & @CRLF)

EndFunc


Func PullDownInstallerToLocalDrive()
   ;Copy the installation program to the local drive before launching.
   myLog("The NEW version " & $omDownloadFQFN & FileGetVersion($omDownloadFQFN) & @CRLF)
   myLog("About to execute FileCopy" & _
      " source=" & $omDownloadFQFN & _
	  " dest=" & $omTempFQPN & _
	  " flags=" & $FC_CREATEPATH )
   FileCopy($omDownloadFQFN, $omTempFQPN, $FC_CREATEPATH + $FC_OVERWRITE)
   ;DirCopy(@MyDocumentsDir, @TempDir & "\Backups\MyDocs", $FC_OVERWRITE)
EndFunc
