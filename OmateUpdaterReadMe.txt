
Rob Townley
  For issues and pull requests, create an issue here
  https://bitbucket.org/eceo/omateupdater/issues


  https://github.com/rjt/
  https://bitbucket.org/rjt69
  https://sourceforge.net/projects/ocscripts/
  https://sourceforge.net/u/robertjtownley/profile
  https://FossCoder.blogspot.com

  Omaha, NEbraska
  rob.townley@gmail.com
  FossCoder@gmail.com
  pcTechs@ECEO.us
  robert@EyeConsultantsPC.com





Unless your network drives and folder structure are exactly like our office, this script
will have to be modified and recompiled.  Hopefully, the project will get to a point
that there are just a few variables to modify, but not there yet.  The good news is
that using the "SciTE-Lite" editor, editing AutoIt scripts is extremely easy.

"SciTE-Lite"  (recommended for beginners)
https://www.autoitscript.com/site/autoit-script-editor/

Full blown version of Scintilla and SciTE
http://www.scintilla.org/SciTEDownload.html

SciTE-Lite
Version 3.4.1
    Jun  1 2014 18:45:52
by Neil Hodgson.
 Updated by Jos
Modified version of SC1 which only contains
the AutoIt3 Lexer.
Check the SciTE4AutoIt3 Homepage for a
full version with lots of extras.
December 1998-April 2014.
http://www.scintilla.org