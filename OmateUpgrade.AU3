#include <Constants.au3>
#include <MsgBoxConstants.au3>
#include <FileConstants.au3>
#include <File.au3>
#include <Misc.au3>
Opt("MustDeclareVars", 1) ;0=no, 1=require pre-declaration

;Set the following to the new version of the OfficeMate installation program.
local $omNewVersion = "11.1.9"
local $omDownloadFQFN = "P:\DOWNLOADS\OfficeMate11.1.9\OM_Suite_Programs.exe"
local $omTempFQPN =  "C:\WINDOWS\TEMP\OM\" & $omNewVersion & "\"
local $omOldVersionFQFN = "C:\OfficeMate\omate.exe"

local $dtStart = @YEAR & @MON & @MDAY & "-" & @HOUR & @MIN & @SEC
;$dtStart = "" ;for now, lets just have one logfile per machine.
local $timerStart = TimerInit()
local $LogPath = "P:\LogFiles\" & @ScriptName & "\" & @ComputerName & "\"
local $LogFileVerbose  = $LogPath   & $dtStart & "-" & $omNewVersion & "-verbose.LOG"
local $LogFileLess     = $LogPath   & $dtStart & "-" & $omNewVersion & ".LOG"
;LogFileVerbose has more timing info but timing info
;will make it more difficult to diff Log Files on the
;same machine or different machines.  So log to two files.

#RequireAdmin

Func myLog($s)
   _FileWriteLog($LogFileVerbose, $s)
   FileWriteLine($LogFileLess, $s)
EndFunc

Func PrintParameters()
   local $i
   local $sParams = ""
   for  $i = 0 to $CmdLine[0] step 1
	  $sParams &= $i & "=" & $CmdLine[$i] & "   "

   Next
   myLog($sParams)
EndFunc

Func SetupLogFiles()
   DirCreate($LogPath)
   myLog( "DirCreate(" & $LogPath & ")  Error=" & @error & @CRLF)
   myLog( "LogFileVerbose=" & $LogFileVerbose & @CRLF)
   myLog( "LogFileLess=" & $LogFileLess & @CRLF)
   myLog( "Start Time=" & $dtStart & @CRLF)
   myLog( "ComputerName=" & @ComputerName & @CRLF)
   myLog( "UserName=" & @UserName & @CRLF)
EndFunc


Func AllowOnlyOneInstance()
   If _Singleton(@ComputerName, 2) = 0 Then
	  myLog( "Already running on this machine, aborting." & @CRLF)
	  Exit
   Else
	  myLog( "Good. No other instance of this script found. Continuing" & @CRLF)
   EndIf
EndFunc



Func AmIElevated()
   If IsAdmin() Then
     myLog("Admin rights are detected." & @CRLF)
   Else
     myLog("COULD NOT ELEVATE " & @UserName & "! ADMIN RIGHTS NOT DETECTED!" & @CRLF)
	 Exit
  EndIf
EndFunc


Func CheckNetworkDrives()
   local $r
  Local $sOstatus = DriveStatus( "O:" & "\") ; Find the status of the home drive, generally this is the C:\ drive.
  Local $sPstatus = DriveStatus( "P:" & "\")
  myLog("O:\ = " & $sOstatus & @CRLF & "P:\ = " & $sPstatus & @CRLF)
  if "READY" == $sOstatus And "READY" == $sPstatus then
	 myLog( "Network drives check out OK." & @CRLF )
	 $r = 1
  Else
	 myLog( "Problem with O:\ or P:\ drives!" & @CRLF)
     $r = -1
  EndIf
  Return($r)
EndFunc

Func SanityInfo()
  myLog("@ComputerName=" & @ComputerName & @CRLF)
  myLog("@CPUArch=" & @CPUArch & " " & "@OSArch=" & @OSArch & " " & "@OSBuild=" & @OSBuild  & @CRLF)
  myLog("OSVersion=" & @OSVersion & " OSServicePack=" & @OSServicePack & @CRLF)
EndFunc

Func SanityChecks()
   CheckNetworkDrives()
   Local $omOldVersion = "10.0.8"
   $omOldVersion = FileGetVersion($omOldVersionFQFN)
   myLog("The current OMATE.exe version is: " & $omOldVersion & @CRLF);
   myLog("Sanity Check ... this SHOULD BE 1: " & _VersionCompare($omNewVersion, $omOldVersion) & @CRLF)
EndFunc


Func PullDownInstallerToLocalDrive()
   ;Copy the installation program to the local drive before launching.
   myLog("The NEW version from OM_Suite_Programs.exe :" & FileGetVersion($omDownloadFQFN) & @CRLF)
   myLog("About to execute FileCopy" & _
      " source=" & $omDownloadFQFN & _
	  " dest=" & $omTempFQPN & _
	  " flags=" & $FC_CREATEPATH )
   FileCopy($omDownloadFQFN, $omTempFQPN, $FC_CREATEPATH + $FC_OVERWRITE)
   ;DirCopy(@MyDocumentsDir, @TempDir & "\Backups\MyDocs", $FC_OVERWRITE)
EndFunc

Func UnblockFileDeleteStreams()
   Local $iR = 0
   Local $sRun = @ComSpec & " /c " & "P:\APPS\Streams.exe /accepteula -s -d " & $omTempFQPN ; ; don't forget " " before "/c"
   myLog("RunWait " & $sRun & @CRLF)
   $iR=RunWait($sRun) ; don't forget " " before "/c"
   myLog("RunWait Streams.exe Return=" & $iR & @CRLF)

EndFunc

Func _Au3RecordSetup()
  Opt('WinWaitDelay',100)
  Opt('WinDetectHiddenText',1)
  Opt('MouseCoordMode',0)
  Local $aResult = DllCall('User32.dll', 'int', 'GetKeyboardLayoutNameW', 'wstr', '')
  If $aResult[1] <> '00000409' Then
    MsgBox(64, 'Warning', 'Recording has been done under a different Keyboard layout' & @CRLF & '(00000409->' & $aResult[1] & ')')
  EndIf
EndFunc

Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc


Func LaunchInstaller()
; Launch the officemate installation program.
   myLog("Code generated from Au3Record.exe  " & @CRLF )
   _AU3RecordSetup()

  Run('C:\Windows\Temp\OM\11.1.9\OM_Suite_Programs.exe')
  _WinWaitActivate("OMSuite  OM 11.1/ EW 11.1/ RW 11.1","")

  ;[CLASS:Button; INSTANCE:1]
  ;&Next >

  Local $OMSuite = "OMSuite  OM 11.1/ EW 11.1/ RW 11.1"
  Local $hnd;
  myLog("Waiting FOR: The InstallShield Wizard will install OfficeMate Suite 11.1")
  $hnd = WinWait($OMSuite, _
    "The InstallShield Wizard will install OfficeMate Suite 11.1 on your computer.  To continue, click Next.")
  ControlClick($hnd, _
    "", _
    "[CLASS:Button; TEXT:&Next >; INSTANCE:1]")

  $hnd = WinWait($OMSuite, _
    "1. Closed all applications on all computers and restarted each computer and server (if applicable).")
  ControlClick($hnd, _
    "", _
    "[CLASS:Button; TEXT:&Next >; INSTANCE:1]")

  $hnd = WinWait($OMSuite, _
    "Please read the following license agreement carefully.")
  ControlClick($OMSuite, _
    "", _
    "[CLASS:Button; ID:6; TEXT:&Yes; INSTANCE:2]")

  $hnd = WinWait($OMSuite, _
    "Select the OfficeMate Suite components to install.");
  ControlClick($hnd, _
    "", _
    "[CLASS:Button; INSTANCE:3]")


  myLog("[CLASS:Static; TEXT:Check Setup Information; INSTANCE:5]")
  myLog("Confirmation of OfficeMate Programs.exe locations, DATA File locations, and SQL locations." & @CRLF)

  $hnd = WinWait($OMSuite, _
    "Check Setup Information")
  Local $InstallationLocationsConfirmations = _
      ControlGetText($hnd, _
        "", _
        "[CLASS:Edit; ID:301; INSTANCE:1]")
  myLog("error=" & @error)
  myLog("InstallationLocationsConfirmations=" & $InstallationLocationsConfirmations)
  ControlClick($hnd, _
    "Check Setup Information", _
    "[CLASS:Button; TEXT:&Next >; INSTANCE:1]")

  myLog("Alot of DLL registration occurs")

  myLog("Please wait, registering OfficeMate DLL files...")

  myLog("Waiting to click reboot.")
  $hnd = WinWait($OMSuite, _
    "Setup Completed")
   ControlClick($hnd, _
     "", _
	 "[CLASS:Button; INSTANCE:4]")
   myLog("Reboot Sent.")

#comments-start
  MouseClick("left",428,493,1)
  MouseClick("left",428,493,1)
  MouseClick("left",428,493,1)
  MouseClick("left",428,493,1)
  MouseClick("left",428,493,1)
#comments-end


EndFunc ;LaunchInstaller()

Func MirrorProgramFiles()
;Copy the 3D stuff from x64 to x86 or is it vice-versa:
;If x86_64 system, copy from Program Files (x86) to Program Files.
;"c:\Program Files (x86)\3D-Eye Draw"
;"C:\Program Files (x86)\3D-Eye Draw"
   myLog("@ProgramFilesDir=" & @ProgramFilesDir & @CRLF)
   myLog('DirCopy("C:\Program Files (x86)\3D-Eye Draw", "C:\Program Files\3D-Eye Draw",  $FC_OVERWRITE)' )
   Local $r = -5;
   $r = DirCopy("C:\Program Files (x86)\3D-Eye Draw", "C:\Program Files\3D-Eye Draw",  $FC_OVERWRITE)
   myLog("3D-Eye Draw Copy Returned " & $r & (1 == $r ) ?  ("SUCCESS") : ("FAILURE") & @CRLF)
EndFunc ;Mirrorx86 .. x86_64



SetupLogFiles()
PrintParameters()
AllowOnlyOneInstance()
AmIelevated()

If 0 > CheckNetworkDrives() Then

   Local $P = DriveMapGet("P:")
   myLog( "P:\ maps to " & $P & "    ErrorCode=" & @error & @CRLF)

   Local $O = DriveMapGet("O:")
   myLog( "O:\ maps to " & $O & "    ErrorCode=" & @error & @CRLF)

   ;Map drives

EndIf

SanityInfo()
SanityChecks()
PullDownInstallerToLocalDrive()
UnblockFileDeleteStreams()
LaunchInstaller()
MirrorProgramFiles()


local $executionTime = TimerDiff($timerStart)
myLog( "Execution Time was " & $executionTime & " milliseconds" & @CRLF)
