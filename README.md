# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Scripts and executables to automate the installation and updates of [OfficeMate.net](https://OfficeMate.net)'s Omate.exe and ExamWriter.exe and ReportWriter.exe
* [EyeFinityCommunities.force.com profile](https://eyefinitycommunities.force.com/support/communitychatteranswers)
* Version 20160804 Beta 
* Version 20140616 Alpha
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Not for a novice, but someone comfortable with changing the values of some variables to match your office setup.
* Configure by opening the Omate12*.AU3 and UtilsECEO.AU3 AutoIT files preferrably in SciTE4AutoIt3 which is a portable runtime.
* SciTE4AutoIt3
* Best to run tests on virtual disposable machines.
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Create an [issue here on this project page](https://bitbucket.org/eceo/omateupdater/issues).
* Vote for our suggestions in the [EyeFinity Community](https://eyefinitycommunities.force.com/support/CommunityideaListComment?id=08714000000PYL7AAO)
