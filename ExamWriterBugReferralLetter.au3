#include "P:\APPS\UtilsECEO.AU3"
Opt("MustDeclareVars", 1) ;0=no, 1=require pre-declaration

#region --- Au3Recorder generated code Start (v3.3.9.5 KeyboardLayout=00000409)  ---

#region --- Internal functions Au3Recorder Start ---
Func _Au3RecordSetup()
  Opt('WinWaitDelay',100)
  Opt('WinDetectHiddenText',1)
  Opt('MouseCoordMode',0)
  Local $aResult = DllCall('User32.dll', 'int', 'GetKeyboardLayoutNameW', 'wstr', '')
  If $aResult[1] <> '00000409' Then
    MsgBox(64, 'Warning', 'Recording has been done under a different Keyboard layout' & @CRLF & '(00000409->' & $aResult[1] & ')')
  EndIf
EndFunc

Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc

_AU3RecordSetup()
#endregion --- Internal functions Au3Recorder End ---

Local $rcWW = 0

Local $PassWord = "Put ExamWriter Password Here!"
Run('C:\OfficeMate\ExamWRITER.exe')
_WinWaitActivate("ExamWRITER Login","")
Send("robert{TAB}" & $PassWord)
;ControlClick("ExamWRITER Login", _
    ;"", _
    ;"[CLASS:SSPanelWndClass; INSTANCE:1]")
Send("{ENTER}")

_WinWaitActivate("Confirm Login","You are logging in as")
ControlClick("Confirm Login", "You are logging in as", "[CLASS:Button; INSTANCE:1]")

;Since the "View ExamWRITER Ehancements" may not appear, time out.
; WinWait returns 0 for timeouts.
_WinWaitActivate("ExamWRITER                    Ver: 11.1                         Provider: ","")
$rcWW = WinWait("View ExamWRITER Enhancements", "", 10)
If 0 == $rcWW Then
  myLog("Found View ExamWRITER Enhancements, so will attempt closing." & @CRLF)
  ControlClick("View ExamWRITER Enhancements", "", "[CLASS:ThunderRT6CommandButton; INSTANCE:1]")
  WinClose("View ExamWRITER Enhancements")
  myLog("View ExamWRITER Enhancements" & " should be closed now." & @CRLF)
Else
   myLog("Return Code " & $rcWW & ' from WinWait("View ExamWRITER Enhancements", "", 10)' & @CRLF)
EndIf


;Find Rob Townley in ExamWriter Control Center.
_WinWaitActivate("ExamWRITER                    Ver: 11.1                         Provider: ","Control Center     U")
Send("{SHIFTDOWN}t{SHIFTUP}ownley{TAB}{SHIFTDOWN}r{SHIFTUP}ob{F2}")
#endregion --- Au3Recorder generated code End ---